%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(asql_tests).

%% asql_tests: tests for module asql.

-include_lib("eunit/include/eunit.hrl").


%% API tests.

compile2_test () ->
    {ok, {asq, _Tree,
        "fun (Xs) ->\n\t"
            "[X || X <- Xs,\t(element(1,X) == 42)\n\t\t\t"
                            "andalso ((element(2,X) == '21')\n\t\t\t"
                            "orelse  (element(3,X) == 'c'))\n\t"
            "]\n"
        "end"
    }} = asql:compile (" a:42 b:'21' || c:c ", [a,b,c]).


run2_test () ->
    {ok, Asq} = asql:compile ("p:d || b:q", [p,b]),
    {ok, [{d,42},{42,q}]} = asql:run (Asq, [{d,42},{},{42,42},{42,q}]).


run3_test () ->
    {ok,[{0,1,0},{1,1,1}]} = asql:run (
        "{x:1 || x:0} y:1",
        [x,y,z],
        [{0,1,0}, {1,1,1}]).

%% Internals

%% End of Module.
