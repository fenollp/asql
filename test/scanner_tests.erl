%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(scanner_tests).

%% scanner_tests: tests for module scanner.

-include_lib("eunit/include/eunit.hrl").


%% API tests.

a1_test() ->
	{ok,[{'(',1},
		 {label,1,"'this'"},
		 {':',1},
		 {word,1,[207,128]}, % 'π' in UTF-8
		 {')',1}
	],1} = scanner:string("( this: π )").

a2_test() ->
	{ok,[{'(',1}
	],1} = scanner:string("( //: π )").

a3_test() ->
	{ok,[{'(',1},
		 {'(',1},
		 {')',1},
		 {':',1},
		 {'(',1},
		 {')',1},
		 {')',1}
    ],1} = scanner:string("( ():() )").

a4_test() ->
	{ok,[{label,1,"'lbl'"},
		 {':',1},
		 {sentence,1,"\":\""}
	],1} = scanner:string("lbl:\":\"").

a5_test() ->
	{ok,[{label,1,"'lbl'"},
		 {':',1},
		 {sentence,1,"\"(blb)\""}
	],1} = scanner:string("lbl:\"(blb)\"").


or_test() ->
	{ok,[{'(',1},
		 {label,1,"'this'"},
		 {':',1},
		 {quote,1,"'ez'"},
		 {')',1},
		 {'or',1},
		 {label,1,"'e'"},
		 {':',1},
		 {label,1,"'r'"}
    ],1} = scanner:string("( this: 'ez' ) || e:r").

complex_logic_test() ->
	{ok,[{'(',1},
		 {label,1,"'a'"},
		 {':',1},
		 {label,1,"'b'"},
		 {'or',1},
		 {label,1,"'c'"},
		 {':',1},
		 {label,1,"'d'"},
		 {')',1},
		 {'and',1},
		 {label,1,"'e'"},
		 {':',1},
		 {label,1,"'f'"}
	],1} = scanner:string("(a:b OR c:d) AND e:f").

%% Internals

%% End of Module.
