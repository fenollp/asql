%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(parser_tests).

%% parser_tests: tests for module parser.

-include_lib("eunit/include/eunit.hrl").


%% API tests.

single_pair_test() ->
	{ok,{asql,{pair,"q","this"}}} = parser:parse([
		{'(',1},
		{label,1,"q"},
		{':',1},
		{word,1,"this"},
		{')',1}
	]).

multiple_pairs_test() ->
	{ok, {asql,{'and',{pair,"d","b"},{pair,"b","d"}}}} = parser:parse([
		{'(',1},
		{label,1,"d"},
		{':',1},
		{word,1,"b"},
		{label,1,"b"},
		{':',1},
		{word,1,"d"},
		{')',1}
	]).

pair_without_pair_test() ->
	{ok, {asql,{'and',
		{pair,"a","b"},
		{without,{pair,"e","f"}}}
	}} = parser:parse([
		{label,1,"a"}, {':',1}, {word,1,"b"},
		{'-',1},
		{label,1,"e"}, {':',1}, {word,1,"f"}
	]).

complex_logic_test() ->
	{ok, {asql,
		{'and',
			{'or',{pair,"a","b"},{pair,"c","d"}},
			{pair,"e","f"}
	}}} = parser:parse([ %% "(a:b || c:d) && e:f"
		{'(',1},
		{label,1,"a"}, {':',1}, {word,1,"b"},
		{'or',1},
		{label,1,"c"}, {':',1}, {word,1,"d"},
		{')',1},
		{'and',1},
		{label,1,"e"}, {':',1}, {word,1,"f"}
	]).

and_priotrity_1_test() ->
	{ok,{asql,
		{'and',
			{pair,"a","b"},
			{'or',{pair,"c","d"},{pair,"e","f"}}
	}}} = parser:parse([
		{label,1,"a"}, {':',1}, {word,1,"b"},
		{'and',1},
		{label,1,"c"}, {':',1}, {word,1,"d"},
		{'or',1},
		{label,1,"e"}, {':',1}, {word,1,"f"}]).

and_priotrity_2_test() ->
	{ok,{asql,
		{'and',
			{'or',{pair,"a","b"},{pair,"c","d"}},
			{pair,"e","f"}
	}}} = parser:parse([
		{label,1,"a"}, {':',1}, {word,1,"b"},
		{'or',1},
		{label,1,"c"}, {':',1}, {word,1,"d"},
		{'and',1},
		{label,1,"e"}, {':',1}, {word,1,"f"}]).

%% Internals

%% End of Module.
