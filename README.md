# ASQL • [Bitbucket](https://bitbucket.org/fenollp/asql)

An Advanced Search Query Language compiler to Erlang's list comprehensions.


## Overview

**ASQL** enables you to express set queries (or row-oriented queries) in a Erlang-agnostic
manner.  
Queries are compiled on-the-fly into Erlang code (namely, into list comprehensions),
via the function `fun asql:compile/2` and applied to your data with `fun asql:run/2` or directly
with `fun asql:run/3`.  
This type of search applies to Erlang's records. You have to give the name of each of the record's fields in order for asql to link your ASQ code to the record.  
The syntax is strongly inspired from most common search engines' advanced queries grammar.

Here is an example:

    :::lisp
    ( url:\.html$ && (sizeAbove : 10kB  OR  dateModified : "2013/05/15") )

## Usage

Say we have defined a record as `-record(x, {a,b,c}).`. Meaning `#x{bi,bo,bu}` now expands
to `{x,bi,bo,bu}`. Thus, `[x,a,b,c]` identifies our tuple's fields.

    :::erlang
    1> Asq = "a:oui! || { b:'this is it'   c : 42 }",
    1> {ok, Query} = asql:compile (Asq, [x,a,b,c]),
    1> MyData = [{x,'oui!',ba,be}, {x,bi,bo,42}, {x,so,me,thing}, {etc}],
    1> asql:run (Query, MyData).
    [#x{'oui!',ba,be}]

You have extracted 2 tuples matching the ASQ description!  
Then you can apply the compiled Query to another set of data…

## Grammar

* Binary `:`: denotes a pair (label value).
* Binary `||`, `OR`, `&&`, `AND`: logic selection operators.
* Unary `+`, `-`: whether to include said pair.
* Parenthesis `()` and `{}` have the same meaning.
