%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
Definitions.
And        = (AND|&&)
Or         = (OR|\|\|)
Label      = [a-z][0-9a-zA-Z_]*
Word       = [^:(){}"'\000-\s]+
WS         = ([\000-\s]|//.*)
Sentence   = "([^"]|\\")*"
Quote      = '([^']|\\')*'

%% Note: rule order matters.

Rules.

{WS}+      :   skip_token.

\:         :   {token,{':', TokenLine}}.

\+         :   {token,{'+', TokenLine}}.
\-         :   {token,{'-', TokenLine}}.

{And}      :   {token,{'and', TokenLine}}.
{Or}       :   {token,{'or', TokenLine}}.

\(         :   {token,{'(', TokenLine}}.
\)         :   {token,{')', TokenLine}}.
\{         :   {token,{'{', TokenLine}}.
\}         :   {token,{'}', TokenLine}}.

{Label}    :   {token,{label, TokenLine, quote(TokenChars)}}.
{Word}     :   {token,{word, TokenLine, secure(TokenChars)}}.
{Sentence} :   {token,{sentence, TokenLine, TokenChars}}.
{Quote}    :   {token,{quote, TokenLine, TokenChars}}.



Erlang code.
quote (Item) -> "'" ++ Item ++ "'".
secure (Item) ->
    case erl_scan:string (Item) of
        {ok,[_],_} -> Item
        ; _ -> quote (Item)
    end.


