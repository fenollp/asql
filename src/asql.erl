%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
-module(asql).

%% asql: transforms ASQL queries into list comprehensions.

-export([compile/2, run/2, run/3]).
-export([scan/1, parse/1]).

-ifdef(TEST).
- define(fmt (Format, List), io:format (Format, List)).
-else.
- define(fmt (Format, List), ok).
-endif.

-define(err(E, Fmt, L),
    (fun () -> io:format (Fmt, L), {error, E} end) ()).

-type record_fields() :: [atom()].
-type asql() :: string().
-type data() :: [tuple()].
-type ast() :: tuple().
-type fun_lc() :: string().
-type asq() :: {asq, ast(), fun_lc()}.

%% API

-spec run (asql(), record_fields(), data()) -> data().
run (Asq, RecordFields, Data) ->
    {ok, CAsq} = compile (Asq, RecordFields),
    run (CAsq, Data).

-spec run (asq(), data()) -> {ok, data()} | term().
run ({asq, _Tree, Fun}, Data) ->
    Args = lists:flatten (io_lib:format ("~p", [Data])),
    eval (Fun, Args).


-spec compile (asql(), record_fields()) -> {ok, asq()}
                                    | {error, syntax_error | parse_error | field_unknown}.
compile (Asq, RecordFields) ->
    try
        Fields = quote (RecordFields),
        {ok, {asql, Tree}} = sp (Asq),
        {ok, Fun} = asql_to_erlang (Tree, Fields),
        {ok, {asq, Tree, Fun}}
    catch
        {M=syntax_error, E} ->
            ?err (M, "SyntaxError: ~p.\n", [E]);
        {M=parse_error, {Line, _Module, [Str, List]}} ->
            ?err (M, "ParseError: ~s~s on line ~p.\n", [Str, List, Line]);
        {M=field_unknown, E} ->
            ?err (M, "Error: unknown field ~s.\n", [E])
    end.



-spec scan (asql()) -> {ok, [any()]} | {error, term()}.
scan (String) ->
    Tokens = scanner:string (String),
    ?fmt ("Scanned into:\n\t~p\n", [Tokens]),
    Tokens.

-spec parse ([any()]) -> {ok, ast()} | {error, term()}.
parse (Tokens) ->
    Parsed = parser:parse (Tokens),
    ?fmt ("Parsed into:\n\t~p\n", [Parsed]),
    Parsed.

%% Internals

quote (Atoms) ->
    [(fun (Atom) ->
        L = [H|_] = atom_to_list (Atom),
        case H of
            $'  ->        L
            ; _ -> "'" ++ L ++ "'"
        end
    end) (A)
    || A <- Atoms].

sp (String) ->
    case scan (String) of
        {ok, Tokens, _Line} ->
            case parse (Tokens) of
                {error, Error} ->
                    throw ({parse_error, Error});
                Parsed ->
                    Parsed
            end;
        Error ->
            throw ({syntax_error, Error})
    end.


asql_to_erlang (Asql, R) ->
    Fun = "fun (Xs) ->\n\t"
              "[X || X <- Xs,\t"
        ++        asqlast_to_erlang (Asql, R) ++ "\n\t"
              "]\n"
          "end",
    ?fmt ("Erlang code:\n~s\n", [Fun]),
    {ok, Fun}.


asqlast_to_erlang ({'pair', Lhs, Rhs}, RecordFields) ->
    "element(" ++ index (Lhs, RecordFields, 1) ++ ",X) == " ++ Rhs;
asqlast_to_erlang ({'and', Lhs, Rhs}, Record) ->
    "(" ++ asqlast_to_erlang (Lhs, Record)
        ++ ")\n\t\t\tandalso (" ++ asqlast_to_erlang (Rhs, Record) ++ ")";
asqlast_to_erlang ({'or', Lhs, Rhs}, Record) ->
    "(" ++ asqlast_to_erlang (Lhs, Record)
        ++ ")\n\t\t\torelse  (" ++ asqlast_to_erlang (Rhs, Record) ++ ")";
asqlast_to_erlang ({'without', This}, Record) ->
    "not(" ++ asqlast_to_erlang (This, Record) ++ ")";
asqlast_to_erlang ({'with', This}, Record) ->
    asqlast_to_erlang (This, Record).

index (Item, [], _) ->
    throw ({field_unknown, Item});
index (Item, [Field|RecordFields], I) ->
    case Item == Field of
        true -> integer_to_list (I);
        _ -> index (Item, RecordFields, I + 1)
    end.


% asql:eval ("fun (X) -> [Y+1 || Y<-X] end", "[1,2,3]").
eval (Fun, Args) ->
    Input = "("++ Fun ++") ("++ Args ++").",
    {ok, Scanned, _} = erl_scan:string (Input),
    {ok, Parsed} = erl_parse:parse_exprs (Scanned),
    Bindings = erl_eval:new_bindings (),
    {value, Res, _NewBindings} = erl_eval:exprs (Parsed, Bindings),
    {ok, Res}.

%% End of Module.
