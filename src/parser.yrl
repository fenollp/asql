%% Copyright © 2013 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
%% -*- coding: utf-8 -*-
Nonterminals R KV E binLogic setAppartenance.

Terminals '(' ')' '{' '}' label word ':' sentence quote 'and' 'or' '+' '-'.

Rootsymbol R.

Left 100 'and'.
Left 150 'or'.
Unary 200 setAppartenance.



R -> E:                     {asql, '$1'}.

E -> '(' E ')':             '$2'.
E -> '{' E '}':             '$2'.
E -> binLogic:              '$1'.
E -> setAppartenance:       '$1'.
E -> KV:                    '$1'.

binLogic -> E       E:      {'and', '$1', '$2'}.
binLogic -> E 'and' E:      {'and', '$1', '$3'}.
binLogic -> E 'or'  E:      {'or', '$1', '$3'}.

setAppartenance -> '+' E:   {with, '$2'}.
setAppartenance -> '-' E:   {without, '$2'}.

KV -> label ':' sentence:   {pair, value_of('$1'), value_of('$3')}.
KV -> label ':'    quote:   {pair, value_of('$1'), value_of('$3')}.
KV -> label ':'     word:   {pair, value_of('$1'), value_of('$3')}.
KV -> label ':'    label:   {pair, value_of('$1'), value_of('$3')}.


Expect 5.

Erlang code.
value_of(Token) -> element(3, Token).
